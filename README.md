This is a BDD framework base project which QA can use to start off new project for Functional automation testing using Specflow C#.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you would like to and then click **Clone**.
4. Open the directory you just created to see your repository files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

Install the Visual Studio community version https://visualstudio.microsoft.com/ (2017)
Open above downloaded project.sln in VS 2017
Checklist of the following libraries available 
Gherkins
HTMLReportControl
NUnit
Selenium.Support
Selenium.WebDriver
Specflow
SeleniumWebDriver.ChromeDriver
