﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace Skeleton.Tests.Specification.Helper
{
    [Binding]
    public static class RandomString
    {
        public static string Random(int size)
        {
            var random = new Random((int)DateTime.Now.Ticks);
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public static string RandomInt(int size)
        {
            var random = new Random((int)DateTime.Now.Ticks);
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, size).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomIntStartsWith(int startsWith, int size)
        {
            var random = new Random((int)DateTime.Now.Ticks);
            const string chars = "123456789";
            return $"{startsWith}{new string(Enumerable.Repeat(chars, size - 1).Select(s => s[random.Next(s.Length)]).ToArray())}";
        }

        public static T GetRandom<T>(this IEnumerable<T> list)
        {
            return list.ElementAt(new Random(DateTime.Now.Millisecond).Next(list.Count()));
        }
    }

    public static class SubstringText
    {
        public static string subtext(string text)
        {
            return text.Substring(text.IndexOf("_p") + 2);
        }
    }

    public static class RandomSpecialCharacter
    {
        public static string randomSpecialChar(int length)
        {
            const string valid = "!@#$%^&*()?<>/|{}',:";
            StringBuilder builder = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                builder.Append(valid[rnd.Next(valid.Length)]);
            }
            return builder.ToString();
        }
    }
}
