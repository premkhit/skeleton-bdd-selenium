﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Skeleton.Tests.Specification.Helper
{
    public class Config
    {
        public static readonly int Timeout = Int32.Parse(GetRequiredValue("Timeout"));

        public static string GetRequiredValue(string key)
        {
            string val = null;

            return val ?? ConfigurationManager.AppSettings[key];
        }

    }
} 