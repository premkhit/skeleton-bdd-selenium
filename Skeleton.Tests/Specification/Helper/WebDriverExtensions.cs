﻿using Skeleton.Tests.Specification.Infrastructure;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using TechTalk.SpecFlow;

namespace Skeleton.Tests.Specification.Helper
{
    public static class WebDriverExtensions
    {
        public static IWebDriver Browser { get { return WebBrowser.Current; } }
        public static IWait<IWebDriver> Wait { get { return WebBrowser.Wait; } }

        public static void WaitForDocumentReady(this IWebDriver driver)
        {
            var javascript = driver as IJavaScriptExecutor;
            if (javascript == null)
                throw new ArgumentException("driver", "Driver must support javascript execution");

            Wait.Until((d) =>
            {
                try
                {
                    string readyState = javascript.ExecuteScript(
                        "if (document.readyState) return document.readyState;").ToString();
                    return readyState.ToLower() == "complete";
                }
                catch (InvalidOperationException e)
                {
                    //Window is no longer available
                    return e.Message.ToLower().Contains("unable to get browser");
                }
                catch (WebDriverException e)
                {
                    //Browser is no longer available
                    return e.Message.ToLower().Contains("unable to connect");
                }
                catch (Exception)
                {
                    return false;
                }
            });

        }

        public static void Sleep(this IWebDriver driver, int millisecond)
        {
            Thread.Sleep(millisecond);
        }

        public static IWebElement WaitElement(this IWebDriver driver, By findMethod, bool displayed = true)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            Wait.Until(ExpectedConditions.ElementExists(findMethod));
            if (displayed)
                Wait.Until(ExpectedConditions.ElementIsVisible(findMethod));
            return driver.FindElement(findMethod);
        }

        public static IWebElement WaitElementContainText(this IWebDriver driver, By findMethod, string text)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            Wait.Until(ExpectedConditions.TextToBePresentInElementLocated(findMethod, text));
            return driver.FindElement(findMethod);
        }

        public static void WaitElementToDisappear(this IWebDriver driver, By findMethod, bool displayed = true)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            Wait.Until(ExpectedConditions.InvisibilityOfElementLocated(findMethod));
        }

        public static IReadOnlyCollection<IWebElement> WaitElements(this IWebDriver driver, By findMethod)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            Wait.Until(ExpectedConditions.ElementExists(findMethod));
            Wait.Until(ExpectedConditions.ElementIsVisible(findMethod));

            Wait.Until(d => (FilterVisibleElements(driver.FindElements(findMethod)).Count >= 1));

            return FilterVisibleElements(driver.FindElements(findMethod));
        }

        public static void WaitElementToContainAttribute(this IWebDriver driver, By findMethod, string attributeName,
            string attributeValue, bool displayed = true)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");
            if (attributeName == null)
                throw new ArgumentNullException("attributeName");
            if (attributeValue == null)
                throw new ArgumentNullException("attributeValue");

            Wait.Until(d => (driver.WaitElement(findMethod, displayed).GetAttribute(attributeName).Contains(attributeValue)));
        }

        private static IReadOnlyCollection<IWebElement> FilterVisibleElements(IReadOnlyCollection<IWebElement> elements)
        {
            var visibleElements = elements.ToList();

            foreach (var element in elements)
            {
                if (!element.Displayed)
                    visibleElements.Remove(element);
            }

            return visibleElements;
        }

        public static void Click(this IWebDriver driver, By findMethod, bool avoidFlyout = true, int delayAfterClick = 0, bool avoidAnchoredNav = false)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            // Ensure that the element is enabled before one can click on it
            Wait.Until(d => (driver.WaitElement(findMethod).Enabled));
            var element = driver.WaitElement(findMethod);
            if (avoidFlyout)
            {
                var scrolledElement = (ILocatable)element;
                var point = scrolledElement.LocationOnScreenOnceScrolledIntoView;
                ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(" + point.X + ", " + point.Y + ");");
            }
            if (avoidAnchoredNav)
            {
                var scrolledElement = (ILocatable)element;
                var point = scrolledElement.LocationOnScreenOnceScrolledIntoView;
                ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(" + point.X + ", " + (point.Y - 100) + ");");
            }
            element.Click();

            if (delayAfterClick > 0)
                Thread.Sleep(delayAfterClick);
        }

        public static string Text(this IWebDriver driver, By findMethod)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            Wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(findMethod));
            return driver.FindElement(findMethod).Text;
        }

        public static void ClearThenSendKeys(this IWebDriver driver, By findMethod, string inputText, bool useBackspaceToClear = false, int delayAfterInput = 0)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            // Ensure that the element is enabled before one can click on it
            Wait.Until(d => (driver.WaitElement(findMethod).Enabled));

            var element = driver.FindElement(findMethod);
            if (useBackspaceToClear)
            {
                var numOfChar = element.GetAttribute("value").Length;
                for (int i = 0; i < numOfChar; i++)
                {
                    element.SendKeys("\b"); // for backspace
                }
            }
            else
            {
                element.Clear();
            }
            element.SendKeys(inputText);

            if (delayAfterInput > 0)
                Thread.Sleep(delayAfterInput);
        }

        public static void DeleteThenSendKeys(this IWebDriver driver, By findMethod, string inputText)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            // Ensure that the element is enabled before one can click on it
            Wait.Until(d => (driver.WaitElement(findMethod).Enabled));

            var element = driver.FindElement(findMethod);
            element.SendKeys(Keys.Control + "a");
            element.SendKeys(Keys.Delete);
            element.SendKeys(inputText);
        }
        public static void WaitForUrlToLoad(this IWebDriver driver, string partialUrl)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (partialUrl == null)
                throw new ArgumentNullException("partialUrl");

            Wait.Until(ExpectedConditions.UrlContains(partialUrl));
        }


        public static void SelectByText(this IWebDriver driver, By findMethod, string text, int delayAfterInput = 0)
        {
            if (driver == null)
                throw new ArgumentNullException("driver");
            if (findMethod == null)
                throw new ArgumentNullException("findMethod");

            // Ensure that the element is enabled before one can click on it
            Wait.Until(d => (driver.WaitElement(findMethod).Enabled));

            var element = driver.WaitElement(findMethod);
            var selectedValue = new SelectElement(element);
            selectedValue.SelectByText(text);

            if (delayAfterInput > 0)
                Thread.Sleep(delayAfterInput);
        }

        public static void SelectByText(this IWebElement element, string text)
        {
            if (element == null)
                throw new ArgumentNullException("element");
            if (text == null)
                throw new ArgumentNullException("text");

            // Ensure that the element is enabled before one can click on it
            Wait.Until(d => (element.Enabled));

            var selectedValue = new SelectElement(element);
            selectedValue.SelectByText(text);
        }

        public static IWebElement SelectedOption(this IWebElement element)
        {
            if (element == null)
                throw new ArgumentNullException("element");

            // Ensure that the element is enabled before one can click on it
            Wait.Until(d => (element.Enabled));

            var selectedValue = new SelectElement(element);
            return selectedValue.SelectedOption;
        }

        public static void ClearThenSendKeys(this IWebElement element, string inputText)
        {
            if (element == null)
                throw new ArgumentNullException("element");

            // Ensure that the element is enabled before one can click on it
            Wait.Until(d => (element.Enabled));

            element.Clear();
            element.SendKeys(inputText);
        }

        public static void Click(this IWebElement element, IWebDriver driver, bool avoidFlyout = true)
        {
            if (element == null)
                throw new ArgumentNullException("element");

            if (avoidFlyout)
            {
                var scrolledElement = (ILocatable)element;
                var point = scrolledElement.LocationOnScreenOnceScrolledIntoView;
                ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollBy(0, -400);");
            }
            element.Click();
        }

        public static void WaitForPageToLoad(this IWebDriver driver, string partialUrl)
        {
            Wait.Until(ExpectedConditions.UrlContains(partialUrl));
        }

        public static bool ElementExists(this IWebDriver driver, By by, int timeout = 5)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                wait.Until(ExpectedConditions.ElementIsVisible(by));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}