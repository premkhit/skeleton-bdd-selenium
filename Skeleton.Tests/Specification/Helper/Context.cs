﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using TechTalk.SpecFlow;

namespace Skeleton.Tests.Specification.Helper
{
    public static class Context
    {
        public static BBWebsite Website
        {
            get
            {
                var tags = FeatureContext.Current.FeatureInfo.Tags;
                if (tags.Contains("BBCM", StringComparer.OrdinalIgnoreCase))
                    return BBWebsite.BBCM;                
                if (tags.Contains("BBCD", StringComparer.OrdinalIgnoreCase))
                    return BBWebsite.BBCD;                
                throw new InvalidOperationException("ERROR: Failed to specify the website.");
            }
        }

       
        public static Device Device
        {
            get
            {
                var tags = FeatureContext.Current.FeatureInfo.Tags;
                if (tags.Contains("Mobile", StringComparer.OrdinalIgnoreCase))
                    return Device.Mobile;
                if (tags.Contains("Desktop", StringComparer.OrdinalIgnoreCase))
                    return Device.Desktop;
                throw new InvalidOperationException("ERROR: Unknown device. Add @Desktop or @Mobile to the feature to indicate the website context for this test.");
            }
        }

        private static bool? runningOnBuildAgent;

        public static bool RunningOnBuildAgent
        {
            get
            {
                if (runningOnBuildAgent == null)
                {
                    var ipAddresses = GetIpAddresses();
                    Console.WriteLine("Local IP addresses: " + string.Join(", ", ipAddresses));
                   // runningOnBuildAgent = ipAddresses.Any(ip => Config.BuildAgents.Contains(ip));
                    Console.WriteLine("Running on build agent: " + runningOnBuildAgent.ToString());
                }
                return runningOnBuildAgent.GetValueOrDefault();
            }
        }

        private static string[] GetIpAddresses()
        {
            return Dns.GetHostAddresses(Dns.GetHostName())
                      .Where(a => a.AddressFamily == AddressFamily.InterNetwork)
                      .Select(a => a.ToString())
                      .ToArray();
        }
    }
}