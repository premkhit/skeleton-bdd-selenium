﻿namespace Skeleton.Tests.Specification.Helper
{
    public enum BBWebsite
    {
        BBCD,
        BBCM,
        None
    }

    public enum Device
    {
        Desktop,
        Mobile
    }
}