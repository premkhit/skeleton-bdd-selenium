﻿using Skeleton.Tests.Specification.Helper;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using NUnit.Framework;
using TechTalk.SpecFlow;
using System.Collections.Generic;

namespace Skeleton.Tests.Specification.Infrastructure
{
    [Binding]
    public sealed class WebBrowser
    {
        public static IWebDriver Current
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("browser"))
                {
                    ScenarioContext.Current["browser"] = CreateWebDriver();
                }
                return (IWebDriver)ScenarioContext.Current["browser"];
            }
        }

        public static string BaseUrl
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("baseurl"))
                {
                    ScenarioContext.Current["baseurl"] = Config.GetRequiredValue("PublicUrl");
                }
                return (string)ScenarioContext.Current["baseurl"];
            }
        }
        public static IWait<IWebDriver> Wait
        {
            get
            {
                if (!ScenarioContext.Current.ContainsKey("wait"))
                {
                    ScenarioContext.Current["wait"] = new WebDriverWait(Current, TimeSpan.FromSeconds(Config.Timeout));
                }
                return (IWait<IWebDriver>)ScenarioContext.Current["wait"];
            }
        }

        [BeforeTestRun]
        public static void MoveMouseAway()
        {
            // Move mouse away from browser area when running tests
            Cursor.Position = new System.Drawing.Point(Cursor.Position.X - 5000, Cursor.Position.Y - 5000);
            KillProcessesIfRunningOnBuildAgent("phantomjs");
            KillProcessesIfRunningOnBuildAgent("firefox");
        }

        [AfterTestRun]
        public static void KillBrowsersAfterTest()
        {
            // Need to kill this process as it will prevent directory clean task for next test run in CI
            KillProcessesIfRunningOnBuildAgent("phantomjs");
            KillProcessesIfRunningOnBuildAgent("firefox");
        }

        [AfterScenario]
        public void AfterScenario()
        {
            // get screenshot and url, if the test involves browser, and save it according to the scenario name
            if (ScenarioContext.Current.TestError != null && ScenarioContext.Current.ContainsKey("browser"))
            {
                Console.WriteLine("INFO: Page url for failing test: " + Current.Url);
                var screenshotDriver = Current as ITakesScreenshot;
                var screenshot = screenshotDriver.GetScreenshot();
                // screenshot.SaveAsFile(TestContext.CurrentContext.TestDirectory + @"\" + ScenarioContext.Current.ScenarioInfo.Title.RemoveSpecialCharacters() + ".jpg", ScreenshotImageFormat.Jpeg);
                screenshot.SaveAsFile(@"C:\Projects\skeleton-bdd-selenium\TestResults\Fail" + @"\" + ScenarioContext.Current.ScenarioInfo.Title.RemoveSpecialCharacters() + ".jpg", ScreenshotImageFormat.Jpeg);
            }
            if (ScenarioContext.Current.ContainsKey("browser"))
            {
                if (Current != null)
                {
                    var screenshotDriver = Current as ITakesScreenshot;
                    var screenshot = screenshotDriver.GetScreenshot();
                    screenshot.SaveAsFile(@"C:\Projects\skeleton-bdd-selenium\TestResults\All" + @"\" + ScenarioContext.Current.ScenarioInfo.Title.RemoveSpecialCharacters() + ".jpg", ScreenshotImageFormat.Jpeg);
                    Current.Manage().Cookies.DeleteAllCookies();
                    Current.Quit();
                }
            }
        }

        private static IWebDriver CreateWebDriver()
        {
            return CreateWebDriver(Config.GetRequiredValue("BrowserType"));
        }

        private static IWebDriver CreateWebDriver(string browser)
        {
            IWebDriver driver;
            switch (browser.ToLower())
            {
                case "chrome":
                    driver = CreateChromeDriver();
                    break;
                case "firefox":
                    driver = CreateFireFoxDriver();
                    break;
                default:
                    throw new Exception("ERROR: Unrecognised Browser Type");
            }
            return driver;
        }

        private static FirefoxDriver CreateFireFoxDriver()
        {
            var options = new FirefoxOptions();
            options.BrowserExecutableLocation = @"C:\Program Files\Mozilla Firefox\firefox.exe";
            if (Context.Device == Device.Mobile)
            {
                options.AddArguments(new string[] { "--simulate-touch-screen-with-mouse",
                    "--user-agent='Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16'"});
            }
            options.SetLoggingPreference(LogType.Browser, LogLevel.All);
            options.AddArgument("--disable-xss-auditor");
            var driver = new FirefoxDriver(options);
            if (Context.Device == Device.Mobile)
            {
                driver.Manage().Window.Size = new Size(480, 800);
            }
            else
            {
                driver.Manage().Window.Size = new Size(1200, 1200);
            }
            return driver;
        }

        private static ChromeDriver CreateChromeDriver()
        {
            var options = new ChromeOptions();
            options.BinaryLocation = @"C:\Users\premkhit.lepcha\AppData\Local\Google\Chrome\Application\chrome.exe";
            if (Context.Device == Device.Mobile)
            {
                options.AddArguments(new string[] { "--simulate-touch-screen-with-mouse",
                    "--user-agent='Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16'"});
            }
            options.SetLoggingPreference(LogType.Browser, LogLevel.All);
            options.AddArgument("--disable-web-security");
            var driver = new ChromeDriver(options);
            if (Context.Device == Device.Mobile)
            {
                driver.Manage().Window.Size = new Size(480, 1200);
            }
            else
            {
                driver.Manage().Window.Size = new Size(1200, 1200);
            }
            return driver;
        }

        //headless chrome browser
        //private static ChromeDriver CreateChromeDriver()
        //{

        //    var chromeOptions = new ChromeOptions
        //    {
        //        BinaryLocation = @"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",

        //    };

        //    chromeOptions.AddArguments(new List<string>() { "headless", "disable-gpu" });

        //    var driver = new ChromeDriver(chromeOptions);

        //    if (Context.Device == Device.Mobile)
        //    {
        //        driver.Manage().Window.Size = new Size(480, 800);
        //    }
        //    else
        //    {
        //        driver.Manage().Window.Size = new Size(1200, 800);
        //    }
        //    return driver;
        //}

        private static void KillProcessesIfRunningOnBuildAgent(string processName)
        {
            if (!Context.RunningOnBuildAgent)
                return;

            try
            {
                foreach (var process in Process.GetProcessesByName(processName))
                    process.Kill();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}