﻿using Skeleton.Tests.Specification.Infrastructure;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace Skeleton.Tests.Specification
{
    [Binding]
    public class BaseSteps : Steps
    {
        public static IWebDriver Browser { get { return WebBrowser.Current; } }
        public static string BaseUrl { get { return WebBrowser.BaseUrl; } }
        public static IWait<IWebDriver> Wait => WebBrowser.Wait;
    }
}