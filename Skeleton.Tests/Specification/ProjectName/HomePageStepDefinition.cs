﻿using System;
using System.Linq;
using System.Net;
using TechTalk.SpecFlow;
using Skeleton.Tests.Specification.SkeletonPages;
using NUnit.Framework;
using OpenQA.Selenium;

namespace Skeleton.Tests.Specification.Skeleton
{

    [Binding]
    public class HomePageStepDefinition : BaseSteps
    {
        private HttpWebRequest task;

        [StepDefinition(@"i see home page ""(.*)"" component")]
        public void ThenISeeHomePageComponent(string component)
        {
            switch (component.ToLower())
            {
                case "search":
                    Assert.IsTrue(Browser.FindElement(HomePage.SearchTextBox()).Displayed);
                    break;

                default:
                    Browser.Navigate().GoToUrl(BaseUrl + component);
                    task = (HttpWebRequest)WebRequest.Create(BaseUrl + component);
                    break;
            }
        }
    }
}