﻿using Skeleton.Tests.Specification.SkeletonPages;
using Skeleton.Tests.Specification.Helper;
using OpenQA.Selenium;
using System.Net;
using TechTalk.SpecFlow;

namespace Skeleton.Tests.Specification.Skeleton
{
    [Binding]
    public class NavigationStepDefinition : BaseSteps
    {
        private HttpWebRequest task; //For Calling the page
        private readonly HttpWebResponse taskresponse = null; //Response returned
        [Then(@"I will be redirected to (.*)")]
        [StepDefinition(@"I (?:navigate to|am at) (.*)")]
        public void WhenINavigate(string pageNameOrUrl)
        {
            switch (pageNameOrUrl.ToLower())
            {
                case "homepage":
                    Browser.Navigate().GoToUrl(BaseUrl);                   
                    break;
                case "mobilehomepage":
                    Browser.Navigate().GoToUrl(BaseUrl);                   
                    break;               
                default:
                    Browser.Navigate().GoToUrl(BaseUrl + pageNameOrUrl);
                    task = (HttpWebRequest)WebRequest.Create(BaseUrl + pageNameOrUrl);
                    break;
            }
        }
    }
}
