﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skeleton.Tests.Specification.SkeletonPages
{

    public static class HomePage
    {
        
        public static By SearchTextBox() { return By.CssSelector("input.gLFyf.gsfi"); }

    }
        
    }   
